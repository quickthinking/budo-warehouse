package org.budo.warehouse.service.api;

import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.warehouse.service.entity.DataNode;

/**
 * @author limingwei
 */
public interface IDataNodeService {
    List<DataNode> listSourceDataNodes(Page pge);

    DataNode findById(Integer id);

    DataNode findByIdCached(Integer id);
}