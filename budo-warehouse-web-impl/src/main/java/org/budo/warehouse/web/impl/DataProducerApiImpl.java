package org.budo.warehouse.web.impl;

import javax.annotation.Resource;

import org.budo.warehouse.logic.api.DataProducerLoader;
import org.budo.warehouse.web.api.DataProducerApi;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Component
public class DataProducerApiImpl implements DataProducerApi {
    @Resource
    private DataProducerLoader dataProducerLoader;

    @Override
    public void loadDataProducer() {
        log.info("#22 loadDataProducer");
        dataProducerLoader.loadDataProducer();
    }

    @Override
    public void restartThread() {
        log.info("#28 restartThread");
        dataProducerLoader.restartThread();
    }
}