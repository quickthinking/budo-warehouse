package org.budo.warehouse.service.bean;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.budo.druid.util.DruidUtil;
import org.budo.support.spring.bean.factory.support.BeanBuilder;
import org.budo.support.spring.util.SpringUtil;
import org.budo.warehouse.service.api.ServiceDynamicBeanProvider;
import org.budo.warehouse.service.entity.DataNode;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.alibaba.druid.pool.DruidDataSource;

import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
@Component
public class ServiceDynamicBeanProviderImpl implements ServiceDynamicBeanProvider {
    @Resource
    private ApplicationContext applicationContext;

    @Override
    public DataSource dataSource(DataNode dataNode) {
        String beanId = "DataSource-" + dataNode.getId() + "-" + dataNode.getUrl() + "-" + dataNode.getUsername();

        DruidDataSource dataSource = (DruidDataSource) SpringUtil.getBeanCached(this.applicationContext, beanId);
        if (null != dataSource /* && !dataSource.isClosed() */) { // 为何会close呢
            return dataSource;
        }

        String password = DruidUtil.rsaDecrypt(dataNode.getPassword());

        DruidDataSource druidDataSource = (DruidDataSource) new BeanBuilder() //
                .id(beanId) //
                .parent("abstractDruidDataSource") //
                .propertyValue("url", dataNode.getUrl()) //
                .propertyValue("username", dataNode.getUsername()) //
                .propertyValue("password", password) //
                .registerTo(this.applicationContext) //
                .get();

        // while (!druidDataSource.isEnable()) { }

        if (druidDataSource.isEnable()) {
            log.error("#49 druidDataSource is not enable, druidDataSource=" + druidDataSource);
        }

        return druidDataSource;
    }
}