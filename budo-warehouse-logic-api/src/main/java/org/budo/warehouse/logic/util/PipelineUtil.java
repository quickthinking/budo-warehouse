package org.budo.warehouse.logic.util;

import java.util.Map;

import org.budo.support.lang.util.MapUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.support.spring.expression.util.SpelUtil;
import org.budo.time.Time;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.service.entity.Pipeline;

import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
public class PipelineUtil {
    public static String targetSchema(Pipeline pipeline, DataEntry dataEntry) {
        Map<String, Object> map = MapUtil.stringObjectMap("pipeline", pipeline, //
                "dataEntry", dataEntry, //
                "_now", Time.now().toString("yyyy-MM-dd HH:mm:ss.SSS"));

        if (null == pipeline) {
            log.error("#27 pipeline=" + pipeline + ", dataEntry=" + dataEntry);
            return null;
        }

        String targetSchema = pipeline.getTargetSchema();
        if (StringUtil.startsWith(targetSchema, "#{")) {
            return SpelUtil.merge(targetSchema, map);
        }

        if (!StringUtil.isEmpty(targetSchema)) {
            return targetSchema;
        }

        if (null == dataEntry) {
            log.error("#37 pipeline=" + pipeline + ", dataEntry=" + dataEntry);
            return null;
        }

        return dataEntry.getSchemaName();
    }

    public static String targetTable(Pipeline pipeline, DataEntry dataEntry) {
        Map<String, Object> map = MapUtil.stringObjectMap("pipeline", pipeline, //
                "dataEntry", dataEntry);

        String targetTable = pipeline.getTargetTable();
        if (StringUtil.startsWith(targetTable, "#{")) {
            return SpelUtil.merge(targetTable, map);
        }

        return dataEntry.getTableName();
    }

    /**
     * 可以使用SPEL表达式
     */
    public static String targetSchemaTable(Pipeline pipeline, DataEntry dataEntry) {
        Map<String, Object> map = MapUtil.stringObjectMap("pipeline", pipeline, //
                "dataEntry", dataEntry);

        String targetTable = pipeline.getTargetTable();
        if (StringUtil.isEmpty(targetTable)) {
            targetTable = dataEntry.getTableName(); // 默认 targetTable = 原表
        } else if (StringUtil.startsWith(targetTable, "#{")) {
            targetTable = SpelUtil.merge(targetTable, map);
        }

        String targetSchema = pipeline.getTargetSchema();
        if (StringUtil.startsWith(targetSchema, "#{")) {
            targetSchema = SpelUtil.merge(targetSchema, map);
        }

        if (!StringUtil.isEmpty(targetSchema)) {
            return "`" + targetSchema + "`.`" + targetTable + "`";
        }

        return "`" + targetTable + "`";
    }

    public static String destinationName(Pipeline pipeline) {
        Map<String, Object> map = MapUtil.stringObjectMap("pipeline", pipeline);

        String targetTable = pipeline.getTargetTable();
        if (StringUtil.startsWith(targetTable, "#{")) {
            targetTable = SpelUtil.merge(targetTable, map);
        }

        String targetSchema = pipeline.getTargetSchema();
        if (StringUtil.startsWith(targetSchema, "#{")) {
            targetSchema = SpelUtil.merge(targetSchema, map);
        }

        if (!StringUtil.isEmpty(targetSchema)) {
            return targetSchema + "." + targetTable;
        }

        if (null == targetTable || targetTable.trim().isEmpty()) {
            throw new IllegalArgumentException("#102 targetTable=" + targetTable + ", targetSchema=" + targetSchema + ", pipeline=" + pipeline);
        }

        return targetTable;
    }
}